
# encryption process

1. encode input string as UTF-8 bytes
2. compress encoded data 
3. generate 32 random bytes as AES key
4. encrypt AES key using provided RSA public key
5. encrypt data using AES key
6. save all in single array of bytes (see format below)
7. encode all in base64

# encrypted data format

`byte[]` (UTF8) + `deflate` (optional) + `base64encode`

1. `short` version header as negative number
2. `short` size of next item
3. `byte[]` AES key, RSA encrypted
4. `short` size of next item
5. `byte[]` UTF8 string bytes, AES encrypted

`LEGACY` format does not have version header and starts with AES key length (field #2). To distinguish between these formats, version header is saved as negative number of the same size (`short`).

## compression

Compression is optional, and selected automatically. 

If the compression actually succeeds in compressing input, i.e. result is actually smaller than source, the compressed result is encrypted and saved.

If the compression is meaningless, uncompressed data is used further for encryption.

Compression is typically useful for larger text blocks like indented configuration fragments.

It is typically meaningless for short high-entropy strings like passwords.
