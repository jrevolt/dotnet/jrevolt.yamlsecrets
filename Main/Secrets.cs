﻿using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Text;

#if NETCOREAPP3_1
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Utilities.IO.Pem;
#endif

namespace JRevolt.YamlSecrets
{
  public enum VersionFormat
  {
    LEGACY,
    UNCOMPRESSED,
    COMPRESSED,
  }

  public class Secrets
  {
    public RSA? PrivateKey { get; protected set; }

    public RSA? PublicKey { get; protected set; }

    private RSAEncryptionPadding Padding { get; } = RSAEncryptionPadding.OaepSHA1;

    public Secrets(string key, string? padding = null)
    {
      if (File.Exists(key)) key = ReadPemFile(key);
      var isPublic = key.Contains("PUBLIC KEY");
      PrivateKey = isPublic ? null : CreateKey(key);
      PublicKey = isPublic ? CreateKey(key) : ExportPublicKeyFromPrivate();
    }



    RSA? ExportPublicKeyFromPrivate()
    {
      if (PrivateKey is null) return null;
      var rsa = RSA.Create();
      rsa.ImportRSAPublicKey(PrivateKey.ExportRSAPublicKey(), out _);
      return rsa;
    }

    string ReadPemFile(string path)
    {
      using var reader = new StreamReader(path);
      return reader.ReadToEnd();
    }

    RSA CreateKey(string pem)
    {
      var rsa = RSA.Create();
      rsa.ImportFromPem(pem);
      return rsa;
    }

    public string Encrypt(string value)
    {
      if (PublicKey is null) throw new YamlSecretsException(nameof(PublicKey));

      // generate random key for AES encryption
      var key = new byte[32];
      RandomNumberGenerator.Create().GetBytes(key);
      // encrypt key using asymmetric cipher
      var encryptedKey = PublicKey.Encrypt(key, Padding);
      // encrypt data
      // compression is attempted before encryption
      var data = Encoding.UTF8.GetBytes(value);
      using var compressed = new MemoryStream();
      using (var x = new DeflateStream(compressed, CompressionLevel.Optimal)) x.Write(data);
      var zdata = compressed.ToArray();
      var isCompressed = zdata.Length < data.Length;
      var encryptedData = AesOperation.Encrypt(key, isCompressed ? zdata : data);

      // save
      using var buf = new MemoryStream();
      using var w = new BinaryWriter(buf);

      // version header
      // using negative number to prevent interference with legacy format which starts with key length
      // must be saved as short, same as key length, so that a simple readInt64() can be used to distinguish
      // between legacy key length (positive)
      // and new version id (negative)
      // weird double cast here is needed to actually write `short` type (negation makes it an `int`)
      w.Write((short)-(short)(isCompressed ? VersionFormat.COMPRESSED : VersionFormat.UNCOMPRESSED));

      w.Write((short) encryptedKey.Length);
      w.Write(encryptedKey);
      w.Write((short) encryptedData.Length);
      w.Write(encryptedData);
      w.Close();

      return Convert.ToBase64String(buf.ToArray());
    }

    private VersionFormat ResolveVersion(int x)
    {
      try
      {
        return (VersionFormat) (Enum.GetValues(typeof(VersionFormat)).GetValue(Math.Abs(x))!);
      }
      catch (IndexOutOfRangeException e)
      {
        throw new YamlSecretsException(e, $"Invalid version format id: {x}");
      }
    }

    public string Decrypt(string encrypted)
    {
      if (PrivateKey is null) throw new YamlSecretsException(nameof(PrivateKey));

      var decoded = Convert.FromBase64String(encrypted);
      using var r = new BinaryReader(new MemoryStream(decoded));

      // read header
      // positive number is a legacy key length field
      // negative number is version format identifier, followed by key length
      var x = r.ReadInt16();
      var version = x < 0 ? ResolveVersion(x) : VersionFormat.LEGACY;
      var keylen = x > 0 ? x : r.ReadInt16();
      var isCompressed = version == VersionFormat.COMPRESSED;

      var encryptedKey = r.ReadBytes(keylen);
      var encryptedValue = r.ReadBytes(r.ReadInt16());
      var key = PrivateKey.Decrypt(encryptedKey, Padding);
      var decryptedData = AesOperation.Decrypt(key, encryptedValue);

      byte[] encoded;
      if (isCompressed)
      {
        using var compressed = new MemoryStream(decryptedData);
        using var decompressed = new MemoryStream();
        using var decompressor = new DeflateStream(compressed, CompressionMode.Decompress);
        decompressor.CopyTo(decompressed);
        encoded = decompressed.ToArray();
      }
      else
      {
        encoded = decryptedData;
      }

      var value = Encoding.UTF8.GetString(encoded);
      return value;
    }

  }

  public static class AesOperation
  {
    public static byte[] Encrypt(byte[] key, byte[] data)
    {
      using Aes aes = Aes.Create();
      aes.Key = key;
      aes.IV = new byte[16];

      using var buf = new MemoryStream();
      using var w = new CryptoStream(buf, aes.CreateEncryptor(), CryptoStreamMode.Write);
      w.Write(data);
      w.Close();

      var encrypted = buf.ToArray();
      return encrypted;
    }

    public static byte[] Decrypt(byte[] key, byte[] encrypted)
    {
      using Aes aes = Aes.Create();
      aes.Key = key;
      aes.IV = new byte[16];

      using var crypto = new CryptoStream(new MemoryStream(encrypted), aes.CreateDecryptor(), CryptoStreamMode.Read);
      using var buf = new MemoryStream();
      crypto.CopyTo(buf);

      var decrypted = buf.ToArray();
      return decrypted;
    }
  }

#if NETCOREAPP3_1
   internal static class RsaExtensions
   {
      internal static void ImportFromPem(this RSA rsa, string pem)
      {
         using var reader = new StringReader(pem);
         var bytes = new PemReader(reader).ReadPemObject()?.Content;
         bool isPublic = pem.Contains("PUBLIC KEY");
         var rsaParameters = isPublic
            ? ToRsaParameters(RsaPublicKeyStructure.GetInstance(Asn1Object.FromByteArray(bytes)))
            : ToRsaParameters(RsaPrivateKeyStructure.GetInstance(bytes));
         rsa.ImportParameters(rsaParameters);
      }

      private static RSAParameters ToRsaParameters(RsaPrivateKeyStructure key)
      {
         var rp = new RSAParameters
         {
            Modulus = key.Modulus.ToByteArrayUnsigned(),
            Exponent = key.PublicExponent.ToByteArrayUnsigned(),
            P = key.Prime1.ToByteArrayUnsigned(),
            Q = key.Prime2.ToByteArrayUnsigned(),
         };
         rp.D = ConvertRsaParametersField(key.PrivateExponent, rp.Modulus.Length);
         rp.DP = ConvertRsaParametersField(key.Exponent1, rp.P.Length);
         rp.DQ = ConvertRsaParametersField(key.Exponent2, rp.Q.Length);
         rp.InverseQ = ConvertRsaParametersField(key.Coefficient, rp.Q.Length);
         return rp;
      }

      private static RSAParameters ToRsaParameters(RsaPublicKeyStructure key)
      {
         var rp = new RSAParameters
         {
            Modulus = key.Modulus.ToByteArrayUnsigned(),
            Exponent = key.PublicExponent.ToByteArrayUnsigned(),
         };
         return rp;
      }


      private static byte[] ConvertRsaParametersField(BigInteger n, int size)
      {
         var bs = n.ToByteArrayUnsigned();
         if (bs.Length == size) return bs;
         if (bs.Length > size) throw new ArgumentException("Specified size too small", nameof(size));

         var padded = new byte[size];
         Array.Copy(bs, 0, padded, size - bs.Length, bs.Length);
         return padded;
      }


   }
#endif
}