﻿using System.Linq;
using System.Reflection;
using System.Security.Cryptography;

namespace JRevolt.YamlSecrets
{
  public static class Utils
  {
    public static RSAEncryptionPadding? ToRsaEncryptionPadding(string? padding)
    {
      if (padding is null) return null;
      var paddingLowercase = padding.ToLower();
      var targetType = typeof(RSAEncryptionPadding);
      return targetType
        .GetProperties(BindingFlags.Public | BindingFlags.Static)
        .Where(x => targetType.IsAssignableFrom(x.PropertyType))
        .Where(x => x.Name.Equals(padding) || x.Name.ToLower().Equals(paddingLowercase))
        .Select(x => (RSAEncryptionPadding) x.GetValue(null)!)
        .First();
    }
  }
}