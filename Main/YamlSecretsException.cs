﻿using System;
using System.Runtime.Serialization;

namespace JRevolt.YamlSecrets
{
  [Serializable]
  public class YamlSecretsException : ApplicationException
  {
    public YamlSecretsException()
    {
    }

    protected YamlSecretsException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    public YamlSecretsException(string message) : base(message)
    {
    }

    public YamlSecretsException(Exception innerException, string message) : base(message, innerException)
    {
    }
  }
}