﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Core;
using YamlDotNet.Core.Events;

namespace JRevolt.YamlSecrets
{
  public abstract class YamlProcessor
  {

    public async Task<string> ProcessYaml(string yaml)
    {
      var buf = new StringWriter();
      await Run(new StringReader(yaml), buf);
      return buf.ToString();
    }

    public async Task Run(TextReader input, TextWriter output)
    {
      var content = await input.ReadToEndAsync();
      var scanner = new Scanner(new StringReader(content));
      var parser = new Parser(scanner);
      await using var writer = output;
      var pos = 0;
      while (parser.MoveNext())
      {
        var x = parser.Current!;
        var data = content.Substring(x.Start.Index, x.End.Index - x.Start.Index);
        await writer.WriteAsync(content.Substring(pos, x.Start.Index - pos));
        var result = IsMatch(x) ? ProcessEvent(x, data) : data;
        await writer.WriteAsync(result);
        pos = x.End.Index;
      }
      await writer.WriteAsync(content[pos..]);
    }

    protected abstract bool IsMatch(ParsingEvent parsingEvent);

    protected abstract string ProcessEvent(ParsingEvent parsingEvent, string data);

    protected Exception Fail(Exception e, ParsingEvent parsingEvent, object data, string msg = "")
    {
      var pos = $"[{parsingEvent.Start.Line}:{parsingEvent.Start.Column}]";
      throw new YamlSecretsException(e, $"{pos} : {msg}. Data: {data}");
    }
  }

  public class YamlEncryptor : YamlProcessor
  {
    private readonly Secrets secrets;

    public YamlEncryptor(Secrets secrets) => this.secrets = secrets;

    protected override bool IsMatch(ParsingEvent parsingEvent) =>
      parsingEvent is Scalar scalar &&
      scalar.Tag != null! &&
      scalar.Tag.Equals("secret");

    protected override string ProcessEvent(ParsingEvent parsingEvent, string data)
    {
      try
      {
        var value = ((Scalar) parsingEvent).Value;
        var encrypted = secrets.Encrypt(value);
        var buf = new StringBuilder();
        buf.Append("!<encrypted> ")
          .Append('"')
          .Append(encrypted)
          .Append('"');
        var needsEol = data.EndsWith("\r") || data.EndsWith("\n");
        if (needsEol) buf.Append('\n');
        return buf.ToString();
      }
      catch (Exception e)
      {
        throw Fail(e, parsingEvent, data);
      }
    }
  }

  public class YamlDecryptor : YamlProcessor
  {
    private readonly Secrets secrets;

    public YamlDecryptor(Secrets secrets) => this.secrets = secrets;

    protected override bool IsMatch(ParsingEvent parsingEvent) =>
      parsingEvent is Scalar scalar &&
      scalar.Tag != null! &&
      scalar.Tag.Equals("encrypted");

    protected override string ProcessEvent(ParsingEvent parsingEvent, string data)
    {
      try
      {
        var value = ((Scalar) parsingEvent).Value;
        var decrypted = secrets.Decrypt(value);

        string Escape(string src) => src
          .Replace("\n", "\\n")
          .Replace("\"", "\\\"")
        ;

        var escaped = $"!<secret> \"{Escape(decrypted)}\"";
        return escaped;
      }
      catch (CryptographicException e)
      {
        throw Fail(e, parsingEvent, data, "Decryption failed! Incorrect private key?");
      }
      catch (EndOfStreamException e)
      {
        throw Fail(e, parsingEvent, data, "Invalid/broken !<encrypted> block, or wrong private key!");
      }
      catch (Exception e)
      {
        throw Fail(e, parsingEvent, data);
      }
    }
  }

}