﻿using System.Collections.Generic;
using System.Security.Cryptography;
using FluentAssertions;
using NUnit.Framework;

namespace JRevolt.YamlSecrets.Test
{
  public class UtilsTests
  {
    public static IEnumerable<object[]> Paddings()
    {
      yield return new object[] {nameof(RSAEncryptionPadding.Pkcs1), RSAEncryptionPadding.Pkcs1};
      yield return new object[] {nameof(RSAEncryptionPadding.OaepSHA1), RSAEncryptionPadding.OaepSHA1};
      yield return new object[] {nameof(RSAEncryptionPadding.OaepSHA256), RSAEncryptionPadding.OaepSHA256};
      yield return new object[] {nameof(RSAEncryptionPadding.OaepSHA384), RSAEncryptionPadding.OaepSHA384};
      yield return new object[] {nameof(RSAEncryptionPadding.OaepSHA512), RSAEncryptionPadding.OaepSHA512};
      // case insensitive
      yield return new object[] {"oaepsha1", RSAEncryptionPadding.OaepSHA1};
    }

    [Test]
    [TestCaseSource(nameof(Paddings))]
    public void Padding(string name, RSAEncryptionPadding padding)
    {
      Utils.ToRsaEncryptionPadding(name).Should().Be(padding);
    }
  }
}