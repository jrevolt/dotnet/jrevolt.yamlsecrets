using System.Diagnostics;
using System.IO;
using FluentAssertions;
using NUnit.Framework;

namespace JRevolt.YamlSecrets.Test
{
   public class SecretsTests
   {
      private string keyfile;
      private string pubfile;

      [SetUp]
      public void Prepare()
      {
         keyfile = Path.GetTempFileName();
         Process.Start("openssl", $"genrsa -out {keyfile} 4096")
            ?.WaitForExit();
         pubfile = Path.GetTempFileName();
         Process.Start("openssl", $"rsa -RSAPublicKey_out -in {keyfile} -out {pubfile}")
            ?.WaitForExit();
      }

      [Test]
      public void PrivateKeyFileLoads()
      {
        var secrets = new Secrets(keyfile);
         secrets.PrivateKey.Should().NotBeNull();
         secrets.PublicKey.Should().NotBeNull();
      }

      [Test]
      public void PrivateKeyPropertyLoads()
      {
         using var reader = new StreamReader(keyfile);
         var key = reader.ReadToEnd();
         var secrets = new Secrets(key);
         secrets.PrivateKey.Should().NotBeNull();
         secrets.PublicKey.Should().NotBeNull();
      }

      [Test]
      public void PublicKeyFileLoads()
      {
         new Secrets(pubfile).PublicKey.Should().NotBeNull();
      }

      [Test]
      public void PublicKeyPropertyLoads()
      {
         using var reader = new StreamReader(pubfile);
         var key = reader.ReadToEnd();
         new Secrets(key).PublicKey.Should().NotBeNull();
      }

      [Test]
      public void Encrypt()
      {
         var secrets = new Secrets(pubfile);
         secrets.Encrypt("heyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyheyhey").Should().NotBeNull();
      }

      [Test]
      public void Decrypt()
      {
        var data = "topsecrettopsecrettopsecrettopsecrettopsecrettopsecrettopsecrettopsecrettopsecrettopsecrettopsecrettopsecrettopsecrettopsecret";
        var secrets = new Secrets(keyfile);
        var encrypted = secrets.Encrypt(data);
        secrets.Decrypt(encrypted).Should().Be(data);
      }

      [Test]
      public void DecryptLegacy()
      {
        var encrypted = "gACKEk6h/McjVJbKr/c5MXHzRhNfUbEDJOZBeXanWVR4JJfMFtXZwg91pSYxTgei+/Bb+OOumWydv8+PpgqcR/OaxS8OlLgZA38t9egZIULTDTPitfymsrTrLfAdIN8wpwXC8gTZ+TMIh8IsIpxHj+KMxdFvVwPpRId5xbZl2Lk/zhAAiXuIUmE75/u4nDY0znw2Aw==";
        var secrets = new Secrets($@"
-----BEGIN RSA PRIVATE KEY-----
MIICWwIBAAKBgQCnQ/UsoHQg9v6pgQeNCPEIRI/73cUNak81YxZfNIkoTe00QaM1
yEJrPWRqm+nuUftkcragcGRzi+C6A8ng78/0rf1i45bBNJ3Pdzy8QqkBFysrmUoD
Vq193T77mbDMxjS6FyXFsvmttOsU4Db5PzmEjIzFGyLiMX2cWoKzGkIvpQIDAQAB
AoGAVtrcAgtjogZ/4Kbms96TwrZPifrzqu6sKM3GPTO7kRk99ierR28wxhz/+rfx
dlZN0zAaEx+pmbJlxFHZT5tXgSA5ywHakf9bP8VnXfg17dq7OB0Lq2FnU02qY6L2
GglqYGbqpy2JnPuRwh4PDga0XtLtmWh4B0imyrebM4zkEVUCQQDBKztaKyF0sNwI
gstFD56dAIhhRWDgElba0WtgPFwuFnmUQX3iKgtfFO+yTXl1fHw1adueucFx6NT/
6TnkxeUXAkEA3avNfpdS2s8w06IAdc2D9nBGZ1BkG7pyyrAsXg/CC2XvjxPte8FQ
lhqHEG4PmyQHA6AVteb5fZXSU4lUMoB+owJAGImqV3DlJhKVUMt0kMhEKMN5j4ml
nbEWsWMq1aW1O/GE7i1HlWQr358kN60Zwa/Xg114FXiZNyQRcpSo0wPEbwJAYi/o
TQtwOtWNdiCK/aMzsaQXu0mDnjifAiK49E0ckXnse77C8Y82R9amPuPRa9Gwfa2a
EEGn/zWuvSvWLKx36wJAKYbajHHWgbsMQXZOgomfgAaBZSBwluBCndmopC1RZ1fk
/wNjjMbHl5ujH+tz+S3DNP4QHxqJfzYtYpNrptTqaw==
-----END RSA PRIVATE KEY-----
");
        secrets.Decrypt(encrypted).Should().Be("hey");
      }

   }


}
