﻿using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using YamlDotNet.Core;

namespace JRevolt.YamlSecrets.Test
{
  public class YamlProcessorTests
  {
    private Secrets secrets;

    [SetUp]
    public void Prepare()
    {
      var keyfile = Path.GetTempFileName();
      Process.Start("openssl", $"genrsa -out {keyfile} 4096")
        ?.WaitForExit();
      secrets = new Secrets(keyfile);
    }

    [Test]
    public async Task Encrypt()
    {
      var result = await new YamlEncryptor(secrets).ProcessYaml("{foo: !<secret> bar}");
      result.Should().Contain("!<encrypted>");
    }

    [Test]
    public async Task Decrypt()
    {
      var encrypted = secrets.Encrypt("hey");
      var result = await new YamlDecryptor(secrets).ProcessYaml("{foo: !<encrypted> "+encrypted+"}");
      result.Should().Contain("!<secret>");
    }

    [Test]
    public async Task Roundtrip()
    {
      var encrypted = await new YamlEncryptor(secrets).ProcessYaml("{foo: !<secret> bar}");
      var result = await new YamlDecryptor(secrets).ProcessYaml(encrypted);
      result.Should().NotBe(encrypted);
      result.Should().Contain("bar");
    }
  }
}