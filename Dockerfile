FROM mcr.microsoft.com/dotnet/sdk:5.0-alpine as build
WORKDIR /work
ADD . .
ARG NUGET_URL=https://repo.jrevolt.io/repository/nuget/index.json
ARG NUGET_USER
ARG NUGET_PASS
RUN dotnet nuget add source $NUGET_URL -n jrevolt -u $NUGET_USER -p $NUGET_PASS --store-password-in-clear-text
RUN dotnet restore
ARG VERSION
RUN dotnet publish Cli -o /app -f net5.0 -p:Version=$VERSION

FROM mcr.microsoft.com/dotnet/runtime:5.0-alpine as runtime
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false \
    PATH="/opt/yamlsecrets:$PATH" \
    X_CONFIGURATION__APPLICATIONDIRECTORY=/root/.yamlsecrets
ARG MAIN=JRevolt.YamlSecrets.Cli
ARG ALIAS=yaml-secrets
ENTRYPOINT ["yaml-secrets"]
RUN apk add icu-libs openssl &&\
    mkdir -p /opt/yamlsecrets $X_CONFIGURATION__APPLICATIONDIRECTORY &&\
    cd /opt/yamlsecrets &&\
    ln -s $MAIN $ALIAS
WORKDIR /work
COPY --from=build /app /opt/yamlsecrets
